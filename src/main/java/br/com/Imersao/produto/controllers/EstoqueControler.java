package br.com.Imersao.produto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.Imersao.produto.dto.Categoria;
import br.com.Imersao.produto.dto.Produto;
import br.com.Imersao.produto.dto.Valor;
import br.com.Imersao.produto.services.ProdutoService;

/* @RestController --> faz o mapeamento que a classe é um controller
 * @RequestMapping --> faz a anotação da do caminho da ação
 * @GetMapping --> Faz a anotação do metodo a ser executado
	 * 	@PathVariable --> Define a variavel que irá ser repassada no metodo na URI]
	 * 	@RequestParam --> informa que esse 
 * @PostMapping --> Anotação do envio das informações para o metodo 
 */

@RestController
@RequestMapping("/estoque")
public class EstoqueControler {

	@Autowired
	private ProdutoService produtoService;

	// realiza a consulta de uma lista de produto ou por uma categoria especifica
	@GetMapping("/produtos")
	public Iterable<Produto> getProdutos(@RequestParam(required = false) Categoria categoria){
		if (categoria == null) {
			return produtoService.getProdutos();
		}
		return produtoService.getProdutos(categoria);
	}
	
	//Realiza a inclusão de um novo
	@PostMapping("/produtos")
	public Produto setProduto(@RequestBody Produto produto) {
		return produtoService.setProduto(produto);
	}

	//Consulta o produto a partir de um produto
	@GetMapping("/produto/{idProduto}")
	public Produto getProduto(@PathVariable int idProduto) {

		Produto retorno = produtoService.getProduto(idProduto);
		if (retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return retorno;
	}
	
	//metodo de alteração do valor do preço do produto
	@PatchMapping("/produto/{idProduto}")
	public Produto mudarValor(@PathVariable int idProduto, @RequestBody Valor valor) {

		Produto retorno = produtoService.mudarValor(idProduto, valor);

		if (retorno == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return retorno;
	}
	
	// metodo para apagar registro de produto no banco de dados
	@DeleteMapping("/produto/{idProduto}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void apagarProduto(@PathVariable int idProduto){
		produtoService.apagarProduto(idProduto);
	}
	

}
